const functions = require("firebase-functions");
const app = require("express")();
const FBauth = require("./util/FBauth");

const {
  getAllScreams,
  postOneScream,
  getScream,
  commentOnScream,
  likeScream,
  unlikeScream
} = require("./handlers/screams");
const {
  signup,
  login,
  uploadImage,
  addUserDetails,
  getAuthenticatedUser
} = require("./handlers/users");

// scream routes
app.get("/screams", getAllScreams);
app.post("/scream", FBauth, postOneScream);
app.get("/scream/:screamId", getScream);
app.post("/scream/:screamId/comment", FBauth, commentOnScream);
app.get("/scream/:screamId/like", FBauth, likeScream);
app.get("/scream/:screamId/unlike", FBauth, unlikeScream);

// TODO delete scream
// TODO like a scream
// TODO unlike a scream

// user routes
app.post("/signup", signup);
app.post("/login", login);
app.post("/user/image", FBauth, uploadImage);
app.post("/user", FBauth, addUserDetails);
app.get("/user", FBauth, getAuthenticatedUser);

exports.api = functions.region("europe-west1").https.onRequest(app);
